import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home.vue'
import Menu from '@/components/Menu.vue'
import About from '@/components/About.vue'
import Login from '@/components/Login.vue'
import Register from '@/components/Register.vue'
import Menua from '@/components/Menua.vue'
import Detials from '@/components/Detials.vue'
import Menub from '@/components/Menub.vue'
import Detialsb from '@/components/Detialsb.vue'


import One from '@/components/TwoComponents/One.vue'
import Two from '@/components/TwoComponents/Two.vue'
import Three from '@/components/TwoComponents/Three.vue'
import Four from '@/components/TwoComponents/Four.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
    	path:'/home',
    	name:'Home',
    	component:Home
    },
    {
    	path:'/menu',
    	name:'Menu',
    	component:Menu
    },
    {
    	path:'/about/:name/:age',
    	name:'About',
    	component:About
    },
    {
    	path:'/login',
    	name:'Login',
    	component:Login
    },
    {
    	path:'/menua',
    	name:'Menua',
    	component:Menua
    },
    {
    	path:'/menub',
    	name:'Menub',
    	component:Menub
    },
    {
    	path:'/detials/:aid',
    	name:'Detials',
    	component:Detials
    },
    {
    	path:'/detialsb/:bid',
    	name:'Detialsb',
    	component:Detialsb
    },
    {
    	path:'/register',
    	name:'Register',
    	component:Register,
    	redirect:'/register/one',
    	children:[
    		{
		    	path:'one',
		    	name:'One',
		    	component:One
		    },
		    {
		    	path:'two',
		    	name:'Two',
		    	component:Two
		    },
		    {
		    	path:'three',
		    	name:'Three',
		    	component:Three
		    },
		    {
		    	path:'three',
		    	name:'Three',
		    	component:Three
		    },
		    {
		    	path:'four',
		    	name:'Four',
		    	component:Four
		    }
    	],
    },
    {
    	path:'*',
    	redirect:'/home'
    }
  ],
	mode:'history',
  linkActiveClass:"myactive"
})
